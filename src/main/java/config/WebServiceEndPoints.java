package config;

public enum WebServiceEndPoints {
    TRELLO("https://api.trello.com/1/boards/");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
