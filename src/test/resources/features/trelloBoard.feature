Feature: Trello Board Testing

  @POST
  Scenario: Post a board with invalid name
    Given the user want to create a board with name ""
    When the user submits post request
    Then bad request should appear

  @POST
  Scenario: Post a new board
    Given the user want to create a board with name "board"
    When the user submits post request
    Then "board" board should be successfully created

  @GET
  Scenario: Get a new board
    Given the user searches for the board he created
    When the user submits get request
    Then the correct board should appear

  @UPDATE
  Scenario: Update a board
    Given user updates board name to "updatedName"
    When the user updates the new details
    Then the board should be successfully updated

  @DELETE
  Scenario: Delete a board
    Given user provides existing board details
    When the user submits delete request
    Then the board should be deleted