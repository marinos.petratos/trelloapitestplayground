package helpers;

import org.json.JSONObject;

import static apirequests.TrelloRequests.KEY;
import static apirequests.TrelloRequests.TOKEN;

public class BoardDetailsHelper {
   public static JSONObject board = new JSONObject();

    public void addBoardDetails(String name) {
        JSONObject boardObject = new JSONObject();
        boardObject.put("key", KEY);
        boardObject.put("token", TOKEN);
        boardObject.put("name", name);
        board=boardObject;
    }

    public void updateBoardDetails(String updateName) {
        board.remove("name");
        board.put("name", updateName);
    }
}
