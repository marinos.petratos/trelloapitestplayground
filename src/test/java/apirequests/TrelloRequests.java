package apirequests;

import config.WebServiceEndPoints;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.junit.Assert;

import static helpers.BoardDetailsHelper.board;
import static net.serenitybdd.rest.SerenityRest.given;

public class TrelloRequests {
    public static Response response;
    RequestSpecification requestSpec;
    public static JSONObject responseBoard = new JSONObject();
    public static final String KEY = "4a3c7536015c9c2819b28fe0f2aa40e3";
    public static final String TOKEN = "10e309a0f94f6178225b41c3dfd0134929f8c5f3a179107db32749c201174db8";

    public void buildUrl(Boolean isPost) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri(WebServiceEndPoints.TRELLO.getUrl());
        if (isPost) {
            builder.setBody(board.toString());
        }
        builder.setContentType("application/json");
        requestSpec = builder.build();
    }

    public void post() {
        response = given().spec(requestSpec)
                .when()
                .post(WebServiceEndPoints.TRELLO.getUrl());
    }

    public void put() {
        response = given().spec(requestSpec)
                .when()
                .put(WebServiceEndPoints.TRELLO.getUrl() + "{id}", responseBoard.get("id"));
    }

    public void get() {
        response = given().spec(requestSpec)
                .when()
                .queryParam("key", KEY)
                .queryParam("token", TOKEN)
                .get(WebServiceEndPoints.TRELLO.getUrl() + "{id}", responseBoard.get("id"));
    }

    public void delete() {
        response = given().spec(requestSpec)
                .when()
                .queryParam("key", KEY)
                .queryParam("token", TOKEN)
                .delete(WebServiceEndPoints.TRELLO.getUrl() + "{id}", responseBoard.get("id"));
    }

    public void getBoardDetails() {
        JsonPath jp = response.jsonPath();
        responseBoard.put("id", jp.get("id").toString());
        responseBoard.put("name", jp.get("name").toString());
    }

    public void validateRequest() {
        JsonPath jp = response.jsonPath();
        String responseName = jp.get("name").toString();
        Assert.assertEquals(responseName, board.get("name"));
    }

    public void validateStatus(int expectedStatus) {
        int statusResponse = response.getStatusCode();
        Assert.assertEquals(expectedStatus, statusResponse);
    }

}
