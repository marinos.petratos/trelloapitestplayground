package stepdefinitions;

import apirequests.TrelloRequests;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.BoardDetailsHelper;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;

import static apirequests.TrelloRequests.responseBoard;

public class TrelloSteps {
    @Steps
    TrelloRequests trelloRequests;

    @Steps
    BoardDetailsHelper boardDetailsHelper;

    @Given("the user want to create a board with name {string}")
    public void theUserWantToCreateABoardWithName(String name) {
        boardDetailsHelper.addBoardDetails(name);
    }

    @When("the user submits post request")
    public void theUserSubmitsPostRequest() {
        trelloRequests.buildUrl(true);
        trelloRequests.post();
    }

    @Then("{string} board should be successfully created")
    public void validateResponseShouldAppear(String name) {
        trelloRequests.validateStatus(200);
        trelloRequests.getBoardDetails();
        Assertions.assertThat(responseBoard.get("name")).isEqualTo(name);
    }


    @Given("the user searches for the board he created")
    public void userProvidesABoardId() {
        trelloRequests.buildUrl(false);
    }

    @When("the user submits get request")
    public void theUserSubmitsGetRequest() {
        trelloRequests.get();
    }

    @Then("the correct board should appear")
    public void theCorrectBoardShouldAppear() {
        validateSuccessResponse();
    }


    @Given("user updates board name to {string}")
    public void userUpdatesBoardNameTo(String name) {
        boardDetailsHelper.updateBoardDetails(name);
    }

    @When("the user updates the new details")
    public void theUserUpdatesTheNewDetails() {
        trelloRequests.buildUrl(true);
        trelloRequests.put();
    }

    @Then("the board should be successfully updated")
    public void theBoardShouldBeSuccessfullyUpdated() {
        validateSuccessResponse();
    }

    @Given("user provides existing board details")
    public void userProvidesExistingBoardDetails() {
        trelloRequests.buildUrl(false);
    }

    @When("the user submits delete request")
    public void theUserSubmitsDeleteRequest() {
        trelloRequests.delete();
    }

    @Then("the board should be deleted")
    public void theBoardShouldBeDeleted() {
        trelloRequests.validateStatus(200);
    }

    @Then("bad request should appear")
    public void badRequestShouldAppear() {
        trelloRequests.validateStatus(400);
    }

    private void validateSuccessResponse() {
        trelloRequests.validateStatus(200);
        trelloRequests.validateRequest();
    }
}
