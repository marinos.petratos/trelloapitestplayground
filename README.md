# Getting started with Serenity and Cucumber

This project show you how to get started with REST-API testing using Serenity and Cucumber and RestAssured. 

## Before cloning this project you need:
```
- JDK 11 which you can download from: https://www.oracle.com/java/technologies/javase-jdk11-downloads.html
- apache-maven-3.6.0
```


## Clone with HTTPS

Git:

    git clone https://gitlab.com/marinos.petratos/trelloapitestplayground.git

## The project

This project gives you a basic project setup, along with some RestAssured-based tests (CRUD and Negative Scenario) and supporting classes/helpers.

### The project directory structure
The project has build scripts for Maven and follows the structure below:
```
src
  + main
  + test
    + java                           Helpers and Glue Code for supporting cucumber scenarios
    + resources
      + features                          Feature files

```

## A simple CRUD feature file along with a Negative scenario with the Trello API
The project comes with one feature file, which contains a basic CRUD functionality

This sample project uses Cucumber scenarios while exercising the Trello API `https://api.trello.com/1/` 
of a hypothetical user who is performing a **C** (Create a board) **R** (Getting the details of the board) **U** (Updating the board) **D** (Deleting the board) use case, 
after he first tried to create an **invalid board** (board, with empty name) 

```Gherkin
Scenario: Post a new board
Given the user want to create a board with name "board"
When the user submits post request
Then "board" board should be successfully created
```

The glue code is responsible for orchestrating calls to a layer of more business-focused classes, which perform the actual REST calls.
Example of the Glue code:
```java
@Steps

@Given("the user want to create a board with name {string}")
public void theUserWantToCreateABoardWithName(String name) {
        boardDetailsHelper.addBoardDetails(name);
        }

@When("the user submits post request")
public void theUserSubmitsPostRequest() {
        trelloRequests.buildUrl(true);
        trelloRequests.post();
        }
    }
```

The actual REST calls are performed using RestAssured in the TrelloRequests helper, like here: 

```java
public class TrelloRequests {

  public void buildUrl(Boolean isPost) {
    RequestSpecBuilder builder = new RequestSpecBuilder();
    builder.setBaseUri(WebServiceEndPoints.TRELLO.getUrl());
    if (isPost) {
      builder.setBody(board.toString());
    }
    builder.setContentType("application/json");
    requestSpec = builder.build();
  }

  public void post() {
    response = given().spec(requestSpec)
            .when()
            .post(WebServiceEndPoints.TRELLO.getUrl()); 
  }
}
```

## Executing the tests & Living documentation

The imported SerenityRest `import static net.serenitybdd.rest.SerenityRest.given;` is there to appear the queries in the report.
From the root folder of the project, run `mvn verify` to run the tests, and find the generated report in:
```
target
  + site
    + serenity
      + index.html                           
```
